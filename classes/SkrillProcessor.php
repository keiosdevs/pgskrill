<?php
/**
 * Created by Keios Solutions.
 * User: Jakub Zych
 * Date: 11/18/15
 * Time: 11:41 AM
 */

namespace Keios\PgSkrill\Classes;

use Keios\PaymentGateway\Traits\SettingsDependent;

/**
 * Class SkrillProcessor
 *
 * @package Keios\PgSkrill\Classes
 */
class SkrillProcessor
{
    use SettingsDependent;

    /**
     * @param string        $orderId
     * @param float|integer $amount
     * @param string        $currency
     * @param string|null   $details
     * @param integer       $prepare
     * @param array         $urls
     *
     * @return array
     */
    public function preparePayment(
        $orderId,
        $amount,
        $currency,
        array $urls,
        $details = null,
        $prepare = 1
    ) {

        $this->getSettings();

        $email = $this->settings->get('skrill.email');
        $company = $this->settings->get('skrill.company');
        $logoUrl = $this->settings->get('skrill.logo');

        $params = [
            'pay_to_email'          => $email,
            'recipient_description' => $company,
            'transaction_id'        => $orderId,
            'logo_url'              => $logoUrl,
            'amount'                => $amount,
            'currency'              => $currency,
            'detail1_description'   => $details,
            'prepare_only'          => $prepare,
            'status_url'            => $urls['status_url'],
            'return_url'            => $urls['redirect_url'],
            'cancel_url'            => $urls['cancel_url'],
	    'ondemand_status_url'   => $urls['ondemand_status_url']
        ];

        return $params;
    }


}