<?php
/**
 * Created by Keios Solutions.
 * User: Jakub Zych
 * Date: 11/18/15
 * Time: 8:53 AM
 */

namespace Keios\PgSkrill\Classes;

/**
 * Skrill payment gateway.
 * Skrill API by Jakub Zych and Biju Joseph Tharakan
 *
 * @link http://www.bijujosephtharakan.com
 * @link https://keios.eu
 **/
class SkrillApi // todo or to depreciate - we're using redirect api with SkrillProcessor class
{
    /**
     * Initializes the request parameter
     *
     * @param string $user_email  The Skrill username
     * @param string $secret_word The secret word used in developer settings
     * @param string $merchant_id The account id
     * @param string $mqi         The MQI password
     * @param string $sid         Session ID
     * @param string $url         Payment URL
     * @param string $refundUrl   Refund URL
     */
    public function __construct(
        $user_email,
        $secret_word = null,
        $merchant_id = null,
        $mqi = null,
        $sid,
        $url = 'https://www.skrill.com/app/pay.pl',
        $refundUrl = 'https://www.skrill.com/app/refund.pl'
    ) {

        //Set authorization parameters user email required for each request
        $this->email = $user_email;
        $this->secretWord = $secret_word;
        $this->merchantId = $merchant_id ? $merchant_id : '';
        $this->mqi = $mqi ? $mqi : '';
        $this->sid = $sid;
        // The url to send payment requests
        $this->url = $url;
        $this->refundUrl = $refundUrl;
    }

    public function preparePayment($action = 'prepare', array $params)
    {
        $this->validateParams($params);

        $bag['action'] = $action;
        $bag['email'] = $this->email;
        $bag['password'] = md5($this->mqi);
        $args = array_merge($bag, $params);
        $url = $this->url;

        return $this->processRequest($args, $url);
    }


    public function prepareRefund($action = 'refund', array $params)
    {
        $this->validateParams($params);

        $bag['action'] = $action;
        $bag['email'] = $this->email;
        $bag['password'] = md5($this->mqi);
        $bag['sid'] = $this->sid;
        $args = array_merge($bag, $params);
        $url = $this->refundUrl;

        $this->processRequest($args, $url);
    }

    public function executeTransfer($action = 'transfer', $sid)
    {
        $args['action'] = $action;
        $args['sid'] = $sid;
        $url = $this->url;

        return $this->processRequest($args, $url);
    }

    private function processRequest($args, $url)
    {
        $fields = http_build_query($args);
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        if ($args['action'] == 'refund') {
            $this->parseXml(curl_exec($curl), "refund");
        }
        $response = curl_exec($curl);
        $error = curl_error($curl);
        curl_close($curl);
        if ($error) {
            return $error;
        }

        return $response;
    }

    private function validateParams($params)
    {
        $required = ['amount', 'currency', 'bnf_email', 'subject', 'note'];

        // todo - replace with Validator
        foreach ($required as $param) {
            if (!array_key_exists($param, $params)) {
                throw new \ValidationException(['message' => 'Bad parameters sent to API procesor']);
            }
        }

        return true;
    }

    /**
     * Parse the response from gateway
     *
     * @param array $fields The post parameters from gateway
     *
     * @return boolean true/false
     */
    public function validateResponse($fields)
    {
        // Validate the Skrill signature
        $concatFields = $fields['merchant_id']
            .$fields['transaction_id']
            .strtoupper(md5($this->secretWord))
            .$fields['mb_amount'].$fields['mb_currency']
            .$fields['status'];
        if (strtoupper(md5($concatFields)) == $fields['md5sig'] && $fields['pay_to_email'] == $this->user_email) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Validate the parameters from return url
     *
     * @param array $fields The get parameters in url
     *
     * @return boolean true/false
     */
    public function validateReturnUrl($fields)
    {
        // Validate the Moneybookers signature
        $concatFields = $this->merchantId
            .$fields['transaction_id']
            .strtoupper(md5($this->secretWord));
        if (strtoupper(md5($concatFields)) == $fields['msid']) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Parse the response from gateway
     *
     * @param string $xml  The xml response
     * @param string $type The type of action - refund
     *
     */
    public function parseXml($xml, $type)
    {

        $parsed = simplexml_load_string($xml);
        if (isset($parsed)) {
            if (isset($parsed->error)) {
                $this->error = $parsed->error->error_msg;
            }
            if ($type == "refund") {
                $this->refundResponse = json_decode(json_encode($parsed), true);
            } else {
                $this->response = json_decode(json_encode($parsed), true);
            }
        }
    }
}