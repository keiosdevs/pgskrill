<?php
/**
 * Created by Keios Solutions.
 * User: Jakub Zych
 * Date: 11/19/15
 * Time: 10:33 AM
 */

namespace Keios\PgSkrill\Classes;


/**
 * Class PostSender
 *
 * @package Keios\PgSkrill\Classes
 */
class PostSender
{
    /**
     * @param string $url
     * @param array  $message
     *
     * @return mixed
     */
    public function executePost($url, array $message)
    {
        $messageString = $this->urlfyPost($message);
        $request = curl_init();

        curl_setopt($request, CURLOPT_URL, $url);
        curl_setopt($request, CURLOPT_POST, count($message));
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($request, CURLOPT_POSTFIELDS, $messageString);

        $result = curl_exec($request);

        curl_close($request);

        return $result;
    }

    /**
     * @param array $post
     *
     * @return string
     */
    private function urlfyPost(array $post)
    {
        $messageString = '';
        foreach ($post as $key => $value) {
            $messageString .= $key.'='.$value.'&';
        }
        rtrim($messageString, '&');

        return $messageString;
    }
}