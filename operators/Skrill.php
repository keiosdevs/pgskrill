<?php namespace Keios\PgSkrill\Operators;

use Finite\StatefulInterface;
use Keios\PgSkrill\Classes\PostSender;
use Redirect;
use Keios\PaymentGateway\Models\Settings;
use Keios\PaymentGateway\Support\HashIdsHelper;
use Keios\PaymentGateway\Support\OperatorUrlizer;
use Keios\PgSkrill\Classes\SkrillProcessor;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Keios\PaymentGateway\ValueObjects\PaymentResponse;
use Keios\PaymentGateway\Traits\SettingsDependent;
use Keios\PaymentGateway\Core\Operator;

/**
 * Class Skrill
 *
 * @package Keios\PgSkrill
 */
class Skrill extends Operator implements StatefulInterface
{
    use SettingsDependent;

    /**
     *
     */
    const CREDIT_CARD_REQUIRED = false;

    /**
     * @var string
     */
    public static $operatorCode = 'keios.pgskrill::lang.operators.skrill';

    /**
     * @var string
     */
    public static $operatorLogoPath = '/plugins/keios/pgskrill/assets/img/skrill/logo.png';

    /**
     * @var string
     */
    public static $modeOfOperation = 'api';

    /**
     * @var array
     */
    public static $configFields = [];


    /**
     * @return PaymentResponse
     * @throws \ValidationException
     */
    public function sendPurchaseRequest()
    {

        $apiUrl = 'https://www.skrill.com/app/payment.pl';

        $this->getSettings();
        $payment = $this->getModel();
        $amount = $this->cart->getTotalGrossCost()->getAmountBasic();
        $currency = $this->cart->getTotalGrossCost()->getCurrency()->getIsoCode();
        $orderId = $payment->order->hash_id;

        $urls = $this->prepareUrls();

        $processor = new SkrillProcessor();
        $api = new PostSender();
        $paymentPrepared = $processor->preparePayment($orderId, $amount, $currency, $urls);

        $sid = $api->executePost($apiUrl, $paymentPrepared);

        if ($sid) {
            $url = 'https://www.skrill.com/app/payment.pl?sid='.$sid;
        } else {
            throw new \ValidationException(
                ['error' => 'Could not connect payment provider. Please try again in a moment or select different provider.']
            );
        }

        return new PaymentResponse($this, $url);

    }

    /**
     * @param array $data
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function processNotification(array $data) //todo
    {
        if (array_key_exists('status', $data)) {
            if ($data['status'] == '2' && $this->can(Operator::TRANSITION_ACCEPT)) {
                try {
                    $this->accept();
                } catch (\Exception $ex) {
                    \Log::error($ex->getMessage().$data);
                }
            } elseif ($data['status'] == 'cancel' && $this->can(Operator::TRANSITION_CANCEL)) {
                try {
                    $this->cancel();
                } catch (\Exception $ex) {
                    \Log::error($ex->getMessage().$data);
                }
            } elseif ($data['status'] == '-2' && $this->can(Operator::TRANSITION_REJECT)) {
                try {
                    $this->rejectFromApi();
                } catch (\Exception $ex) {
                    \Log::error($ex->getMessage().$data);
                }
            } else {
                \Log::error($data);
            }

            return \Redirect::to($this->returnUrl);
        } else {
            return \Redirect::to($this->returnUrl);
        }
    }

    /**
     * @return \Keios\PaymentGateway\ValueObjects\PaymentResponse
     */
    public function sendRefundRequest()
    {

    }

    /**
     * @param array $data
     *
     * @return string
     */
    public static function extractUuid(array $data)
    {
        if (isset($data['pgUuid'])) {
            return base64_decode($data['pgUuid']);
        } else {
            throw new \RuntimeException('Invalid redirect, payment uuid is missing.');
        }
    }

    /**
     *
     */
    protected function makeCardErrorMessage()
    {

    }

    /**
     *
     * @return array
     */
    private function prepareUrls()
    {
        $urlizedPayment = OperatorUrlizer::urlize($this);
        $basedPayment = base64_encode($this->uuid);
        $statusUrl = \URL::to('_paymentgateway/').'/'.$urlizedPayment.'?pgUuid='.$basedPayment;
        $cancelUrl = \URL::to('_paymentgateway/').'/'.$urlizedPayment.'?pgUuid='.$basedPayment;
        $redirectUrl = \URL::to($this->returnUrl);

        return [
            'status_url'   => $statusUrl,
            'redirect_url' => $redirectUrl,
            'cancel_url'   => $cancelUrl,
	    'ondemand_status_url' => $cancelUrl
        ];
    }
}
