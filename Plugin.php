<?php namespace Keios\PgSkrill;

use System\Classes\PluginBase;
use Keios\PaymentGateway\Models\Settings as PaymentGatewaySettings;
use Event;

/**
 * PgSkrill Plugin Information File
 */
class Plugin extends PluginBase
{
    public $require = [
        'Keios.PaymentGateway',
    ];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'keios.pgskrill::lang.plugin.name',
            'description' => 'keios.pgskrill::lang.plugin.description',
            'author'      => 'Keios',
            'icon'        => 'icon-money',
        ];
    }

    public function register()
    {

        Event::listen(
            'paymentgateway.booted',
            function () {
                /**
                 * @var \October\Rain\Config\Repository $config
                 */
                $config = $this->app['config'];
                $config->push('keios.paymentgateway.operators', 'Keios\PgSkrill\Operators\Skrill');
            }
        );

        Event::listen(
            'backend.form.extendFields',
            function ($form) {

                if (!$form->model instanceof PaymentGatewaySettings) {
                    return;
                }

                if ($form->context !== 'general') {
                    return;
                }

                /**
                 * @var \Backend\Widgets\Form $form
                 */
                $form->addTabFields(
                    [
                        'skrill.general'     => [
                            'label' => 'keios.pgskrill::lang.settings.general',
                            'tab'   => 'keios.pgskrill::lang.settings.tab',
                            'type'  => 'section',
                        ],
                        'skrill.info'        => [
                            'type' => 'partial',
                            'path' => '$/keios/pgskrill/partials/_skrill_info.htm',
                            'tab'  => 'keios.pgskrill::lang.settings.tab',
                        ],
                        'skrill.email'       => [
                            'label'   => 'keios.pgskrill::lang.settings.email',
                            'tab'     => 'keios.pgskrill::lang.settings.tab',
                            'type'    => 'text',
                            'default' => '',
                        ],
                        'skrill.company' => [
                            'label'   => 'keios.pgskrill::lang.settings.company',
                            'tab'     => 'keios.pgskrill::lang.settings.tab',
                            'type'    => 'text',
                             'default' => '',
                        ],
                        'skrill.logo' => [
                            'label'   => 'keios.pgskrill::lang.settings.logo',
                            'tab'     => 'keios.pgskrill::lang.settings.tab',
                            'type'    => 'text',
                            'default' => '',
                        ],
                    ]
                );
            }
        );
    }
}
