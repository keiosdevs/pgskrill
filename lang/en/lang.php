<?php

return [
    'plugin'    => [
        'name'        => 'PgSkrill',
        'description' => 'Skrill support for payment gateway',
    ],
    'settings'  => [
        'general' => 'General Settings',
        'tab'     => 'Skrill',
        'email'   => 'E-mail',
        'company' => 'Company',
        'logo'    => 'Company logo URL',
    ],
    'info'      => [
        'header' => 'How to retrieve your Skrill credentials',
    ],
    'operators' => [
        'skrill' => 'Skrill',
    ],
];